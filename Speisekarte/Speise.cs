﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speisekarte
{
	// Klasse für die Repräsentation der Speisen
	// Für jede Speise wird in der Klasse mysql ein SPeiseobjekt erstellt 
    public class Speise
    {
		// Private attribute des Objekts
		private int _id;
        private string _name;
		private string _beschreibung;
        private double _preis;
        private bool _bestellen;

		// Konstruktor für das Speisen objekt
		// Die Daten werden in der mysql bei der erstellung der jeweiligen instanz übergeben
		public Speise(int id, string name, string beschreibung, double preis)
		{
			_id = id;
			_name = name;
			_beschreibung = beschreibung;
			_preis = preis;
		}

		// Öffentliche Atribute
		// nur mit getter methoden da sie nur Abgefragt werden
		public int ID
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

		public string Beschreibung
		{
			get { return _beschreibung; }
		}

		public double Preis
		{
			get { return _preis; }
		}

        public bool Bestellen
        {
            get { return _bestellen; }
            set { _bestellen = value; }
        }
    }
}
