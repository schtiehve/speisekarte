using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Speisekarte
{
	public class Bestellung
	// Klasse für die indivuduellen bestellungen jedes Kunden Speisenobjekte werden hinzugefügt
	// ermöglicht eine übersicht über die bestellten speisen
	{
		// Initialisierung der bestellID und der Speisen Liste
		private int _bestellID;
		private List<Speise> _speisen = new List<Speise>();

		// Konstruktor der Bestellung
		public Bestellung(int id)
		{
			_bestellID = id;
		}
		// Getter für die Bestell ID
		public int BestellID
		{
			get { return _bestellID; }
		}

		// Getter für die liste der Speisen
		public List<Speise> speisen
		{
			get { return _speisen; } 
		}


		// Methode für den Preis der Bestellung
		public double getPreis()
		{
			double preis = 0;
			foreach (Speise speise in _speisen)
			{
				preis += speise.Preis;
			}
			return preis;
		}

		// Methode zum hinzufügen von Speisen
		public void addSpeise(Speise Speise)
		{
			this._speisen.Add(Speise);
			// SQL: füge im datensatz eintrag this._bestellID Speise.ID hinzu
		}
	}
}
