﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Speisekarte
{
	public static class mysql
	{
		// Variablen für die Verbindung zum mysql server
        /*
		public static string Servername = "schtiehve.duckdns.org";
		public static string Database = "speisekarte";
		public static string User = "speisekarte";
        public static string Password = "m#15rfX}RGF7";
         */

        // Variablen für die Verbindung zum mysql server 
        public static string Servername = "localhost";
        public static string Database = "speisekarte";
        public static string User = "root";
        public static string Password = "";


		// Connection String zusammengesetzt aus den Veriundungs Variablen
		public static string ConnectionString = "SERVER=" + Servername + ";DATABASE=" + Database + ";UID=" + User + ";PASSWORD=" + Password + ";";

		// Methode um die Datenbank (gefiltert) abzufragen
		// Erstellt Speisenobjekte aus jeder Datebankreihe und fügt sie in eine liste ein
		// Gibt die liste der Speisenobjekte zurück
		public static List<Speise> speisen(string kategorie, string inhaltsstoff)
		{
			// initialisierung der Variablen
			string query;
			List<Speise> result = new List<Speise>();

			MySqlConnection connection = new MySqlConnection(ConnectionString);

			// Öffnen der mysql verbindung
			connection.Open();

			// setzen des query strings abhängig von der gewählten Kategorie
			if (kategorie != "all" && inhaltsstoff != "all") {
				query = "SELECT ID,Speise,Beschreibung,Preis FROM speisekarte as sk JOIN katindex AS ki ON sk.ID = ki.SpeisenID JOIN kategorien AS k ON k.KategorieID = ki.KategorieID WHERE k.Kategorie = \"" + kategorie + "\" ;";
			}
			else if (kategorie != "all" && inhaltsstoff == "all") {
				query = "SELECT ID,Speise,Beschreibung,Preis FROM speisekarte as sk JOIN katindex AS ki ON sk.ID = ki.SpeisenID JOIN kategorien AS k ON k.KategorieID = ki.KategorieID WHERE k.Kategorie = \"" + kategorie + "\" ;";
			}
			else if (kategorie == "all" && inhaltsstoff != "all") {
				query = "SELECT ID,Speise,Beschreibung,Preis FROM speisekarte as sk JOIN inhaltindex AS ii ON sk.ID = ii.SpeisenID JOIN inhaltsstoffe AS i ON i.InhaltsstoffID = ii.InhaltsstoffID WHERE i.Inhaltsstoff = \"" + inhaltsstoff + "\" ;";
			}
			else {
				query = "SELECT ID,Speise,Beschreibung,Preis FROM speisekarte as sk;";
			}

			// Ausführen des Mysql befehls
			MySqlCommand cmd = new MySqlCommand(query, connection);
			MySqlDataReader dataReader = cmd.ExecuteReader();

			// Lesen der Daten und erstellen der Speisenobjekte
			while (dataReader.Read())
			{
				int id;
				string name;
				string beschreibung;
				double preis;
				id = Convert.ToInt32(dataReader["ID"]);
				name = dataReader["Speise"].ToString();
				beschreibung = dataReader["Beschreibung"].ToString();
				preis = Convert.ToDouble(dataReader["Preis"]);
				Speise speise = new Speise(id, name, beschreibung, preis);
				result.Add(speise);
			}
			// beenden des lesens und schließen der Verbindung
			
			dataReader.Close();
			connection.Close();

			// rückgabe der Liste mit speisen als ergebnis
			return result;
		}
	
	}
}
