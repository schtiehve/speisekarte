﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Speisekarte
{
    public partial class Form2 : Form
    {
        public Form2(List<Speise> bestellte, double preis)
        {
            InitializeComponent();
            dataGridView1.DataSource = bestellte;
            label1.Text = preis.ToString();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          printDocument1.Print();
        }
    }
}
