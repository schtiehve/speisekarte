﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Speisekarte

{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
            speisenanzeige.DataSource = mysql.speisen("all", "all");
            //speisenanzeige

        }

        public void Form1_Load(object sender, EventArgs e)
        {

        }

        private void InitializeSpeisenanzeige()
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void speiseBindingSource1_CurrentChanged(object sender, EventArgs e)
        {
            
        }

        private void speisenanzeige_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bestellung dieseBestellung = new Bestellung(1);
            //List<Speise> bestellte = new List<Speise>();
            foreach (DataGridViewRow row in speisenanzeige.Rows)
            {
                if((bool)row.Cells["Bestellen"].Value)
                {
                    int ID = Convert.ToInt32(row.Cells[0].Value);
                    string Name = row.Cells[2].Value.ToString();
                    string Beschreibung = row.Cells[3].Value.ToString();
                    double Preis = Convert.ToDouble(row.Cells[4].Value);
                    Speise bestelltespeise = new Speise(ID, Name, Beschreibung, Preis);
                    dieseBestellung.addSpeise(bestelltespeise);
                }
            }
            Form2 bestellung = new Form2(dieseBestellung.speisen, dieseBestellung.getPreis());
            bestellung.Show();
        }

        private void Form1_Load()
        {

        }
    }
}
