/* drops all the tables */
DROP TABLE IF EXISTS inhaltindex;
DROP TABLE IF EXISTS katindex;
DROP TABLE IF EXISTS speisekarte;
DROP TABLE IF EXISTS inhaltsstoffe;
DROP TABLE IF EXISTS kategorien;
