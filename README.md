# Schulprojekt "Digitale Speisekarte" 11FI5C

Das ist das git repository für das Schulprojekt "Digitale Speiskarte"

## Nutzung

### Download der Dateien

Download der Dateien kann entweder über ein git clone oder download eines snapshots erfolgen. 

### Einrichten der Datenbank

- Datenbank "speisekarte" auf mysql system anlegen (lokal oder remote)
- Importieren der sql Datei in database/speisekarte.sql

### Projekt ausführen

- Öffnen des Projekts mit Visual Studio
- Gegebenenfalls die mysql verbindungsdaten in der mysql klasse anpassen
- Projekt ausführen
